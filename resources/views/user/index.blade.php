@extends('layouts.app')

@section('bodyId') id="user" @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Usuarios
                    <a href="{{ route('user.create') }}" class="btn btn-primary">NUEVO</a>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-striped data-table"></table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
