require('datatables.net-bs');
require('jquery-validation/dist/jquery.validate.js');

$(function () {

    window.table = $('table')
        .DataTable({
            'language': {'url': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'},
            'destroy': true,
            'autoWidth': false,
            'processing': true,
            'serverSide': true,
            'ajax': {
                'url': '/api/users',
                'dataSrc': 'data',
                'dataFilter': function (data) {
                    let json = $.parseJSON(data);
                    json.recordsTotal = json.meta.total;
                    json.recordsFiltered = json.meta.total;
                    return JSON.stringify(json);
                }
            },
            'columns': [
                {'title': 'Nombres', 'data': 'name'},
                {'title': 'Correo Electronico', 'data': 'email'},
                {'title': 'Fecha de regístro', 'data': 'created_at'},
                {'title': 'Foto', 'data': 'image'},
                {'title': 'Acción', 'data': 'id'}
            ],
            'columnDefs': [
                {
                    'targets': 3,
                    'render': function (data, type, row, meta) {
                        var html = '';
                        html =
                        '<img src='+ row.image +' width="100px" height="50px">';
                        return html;
                    }
                },
                {
                    'targets': -1,
                    'render': function (data, type, row, meta) {
                        var html = '';
                        html =
                            '<a href="/user/' + row.id + '/edit" class="btn btn-warning">EDITAR</a>\n'+
                            '<form method="POST" action="/api/users/'+ row.id + '">'+
                            '<input type="hidden" name="_method" value="DELETE">'+
                            '<button type="submit" class="btn btn-danger">ELIMINAR</button>'+
                            '</form>';
                        return html;
                    }
                }
            ]
        })


    let errors = {
        customize: function (error, element) {
            element.prop('placeholder', error.text());
            element.parent().addClass('error');
        }
    };

    $('#form-user').validate({
        rules: {
            name: 'required',
            email: {required: true, email: true},
            password: 'required',
            image : 'required'
        },
        messages: {
            name: 'Nombre es requerido',
            password: 'Contraseña es requerido',
            email: {
                required: 'El email es requerido',
                email: 'Debe ser tipo email',
            },
            image: 'Foto es requerido'
        },
        errorElement: 'em',
        errorPlacement: errors.customize,
        submitHandler: function (form, event) {
            event.preventDefault();
            let formData = new FormData($("#form-user")[0]);
            $.ajax({
                url: $("#form-user").prop('action'),
                type: $("#form-user").prop('method'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {

                }
            });
        }

    })

});
