<?php

namespace App\Http\Resources;

use App\Traits\ResourceCollectionTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UsersResource extends ResourceCollection
{
    use ResourceCollectionTrait;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->responseResourceCollection('data', UserResource::collection($this->collection));
    }
}
