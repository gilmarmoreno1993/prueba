<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Resources\UsersResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, UserService $userService)
    {
        $search = $request->get('search', []);
        empty($search['value']) ?: $search['value'] = htmlentities($search['value']);
        $length = $request->has('length') ? $request->length : 10;
        $page = $request->has('start') ? $request->start / $length + 1 : 1;
        $data = $userService->getAll($search, $request->order, $length, $page);
        return $this->responseCollectionSuccess('', new UsersResource($data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserService $userService)
    {
        $data = $request->except('_token');
        $data['password'] = bcrypt($request->password);
        ($request->has('image')) ? $data['image'] = 'storage/'.$request->file('image')->store('user') : '';
        $user = $userService->create($data);
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id, UserService $userService)
    {
        $user = $userService->find($id);
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, UserService $userService)
    {
        $data = $request->except('_token');
        ($request->has('password')) ? $data['password'] = bcrypt($request->password) : '';
        ($request->has('image')) ? $data['image'] = 'storage/'.$request->file('image')->store('user') : '';
        $user = $userService->find($id)->update($data);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, UserService $userService)
    {
        $user = $userService->find($id)->delete();
        return 'Usuario Eliminado';
    }
}
