<?php

namespace App\Services;

use App\Models\User;

class UserService extends BaseService
{
    /**
     * UserService constructor.
     */
    public function __construct()
    {
        parent::__construct(User::class);
    }

    public function getAll($filters = [], $order = [], $limit = 100, $page = 1, $fields = ['*'])
    {
        $data = $this->getModel()->orderBy('created_at', 'desc');
        if (isset($filters['value'])) {
            $data = $data->where(function ($query) use ($filters) {
                $query->orWhere('name', 'like', '%' . $filters['value'] . '%');
                $query->orWhere('email', 'like', '%' . $filters['value'] . '%');
            });
        }
        if (isset($order)) {
            switch ($order[0]['column']) {
                case 1:
                    $data = $data->orderBy('name', $order[0]['dir']);
                    break;
                case 2:
                    $data = $data->orderBy('email', $order[0]['dir']);
                    break;
                case 3:
                    $data = $data->orderBy('created_at', $order[0]['dir']);
                    break;
            }
        }
        return $data->paginate($limit, ['*'], 'draw', $page);
    }

}
