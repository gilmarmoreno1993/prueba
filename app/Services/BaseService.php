<?php

namespace App\Services;


abstract class BaseService
{

    private $model;


    /**
     * BaseService constructor.
     * @param $model
     */
    protected function __construct($model)
    {
        $this->setModel($model);
    }

    /**
     * @return string
     */
    protected function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    protected function setModel($model)
    {
        $this->model = app()->make($model);
    }


    /**
     * @param int $limit
     * @param array $fields
     * @param int $page
     * @return mixed
     */
    public function getAll($limit = 100, $page = 1, $fields = ['*'])
    {
        return $this->getModel()->paginate($limit, $fields, 'page', $page);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $data = $this->clearNulls($data);
        return $this->getModel()->create($data);
    }

    /**
     * @param $data
     * @param $id
     *
     * @param string $field
     * @return mixed
     */
    public function update($data, $id, $field = 'id')
    {
        $object = $this->getModel()->where($field, '=', $id)->firstOrFail();
        $data = $this->clearNulls($data);
        $object->fill($data);
        $object->save();
        return $object;
    }

    /**
     * @param $value
     * @param string $field
     */
    public function delete($value, $field = 'id')
    {
        $object = $this->getModel()->where($field,'=',$value)->firstOrFail();
        $object->delete();
    }

    /**
     * @param $id
     * @param string $field
     * @return mixed
     */
    public function find($id, $field = 'id')
    {
        return $this->getModel()->where($field, '=', $id)->firstOrFail();
    }

    /**
     * @param $data
     * @return array
     */
    public function clearNulls($data)
    {
        return array_filter($data, 'strlen');
    }

}
